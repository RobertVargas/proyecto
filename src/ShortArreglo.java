import java.util.Arrays;

public class ShortArreglo extends Arreglo {
    private short[] arr;
    private int size;

    ShortArreglo(int capacidad) {
        arr = new short[capacidad];
        size = 0;
    }

    @Override
    public void ordenar() {
        Arrays.sort(arr, 0, size);
        for (int i = 0; i < size / 2; i++) {
            short temp = arr[i];
            arr[i] = arr[size - 1 - i];
            arr[size - 1 - i] = temp;
        }

    }

    @Override
    public void imprimir() {
        System.out.println("Array ordenado: ");
        for (int i = 0; i< size; i++){
            System.out.println(arr[i]+ " ");
        }
        System.out.println();


    }

    public void insertar(short elemento) {
        if (size < arr.length) {
            arr[size++] = (short) elemento;
        }

    }

}

