import java.util.Arrays;

public class ByteArreglo extends Arreglo{
    private byte[] arr;
    private int size;
    ByteArreglo(int capacidad) {
        arr = new byte[capacidad];
        size = 0;
    }

    @Override
    public void ordenar() {
        Arrays.sort(arr, 0, size);
        for (int i = 0; i < size / 2; i++) {
            byte temp = arr[i];
            arr[i] = arr[size - 1 - i];
            arr[size - 1 - i] = temp;
        }
    }

    @Override
    public void imprimir() {
        System.out.println("Array ordenado: ");
        for (int i = 0; i< size; i++){
            System.out.println(arr[i]+ " ");
        }
        System.out.println();

    }

    void insertar(byte elemento) {
        if (size < arr.length) {
            arr[size++] = (byte) elemento;
        }
    }


}
