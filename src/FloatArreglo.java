import java.util.Arrays;

public class FloatArreglo extends Arreglo {
    private float[] arr;
    private int size;

    FloatArreglo(int capacidad) {
        arr = new float[capacidad];
        size = 0;
    }

    @Override
    public void ordenar() {
        Arrays.sort(arr, 0, size);
        for (int i = 0; i < size / 2; i++) {
            float temp = arr[i];
            arr[i] = arr[size - 1 - i];
            arr[size - 1 - i] = temp;
        }

    }

    @Override
    public void imprimir() {
        System.out.println("Array ordenado: ");
        for (int i = 0; i< size; i++){
            System.out.println(arr[i]+ " ");
        }
        System.out.println();


    }

    public void insertar(float elemento) {
        if (size < arr.length) {
            arr[size++] = (float) elemento;
        }
    }

}
