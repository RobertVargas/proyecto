import java.util.Arrays;

public class CharArreglo extends Arreglo {
    private char[] arr;
    private int size;

    CharArreglo(int capacidad) {
        arr = new char[capacidad];
        size = 0;
    }

    @Override
    public void ordenar() {
        Arrays.sort(arr, 0, size);
        for (int i = 0; i < size / 2; i++) {
            char temp = arr[i];
            arr[i] = arr[size - 1 - i];
            arr[size - 1 - i] = temp;
        }


    }

    @Override
    public void imprimir() {
        System.out.println("Array ordenado: ");
        for (int i = 0; i< size; i++){
            System.out.println(arr[i]+ " ");
        }
        System.out.println();

    }

    public void insertar(char elemento) {
        if (size < arr.length) {
            if (Character.isLetter(elemento)) {
                arr[size++] = (char) elemento;
            } else {
                System.out.println("Valor invalido");

            }

        }
    }
}