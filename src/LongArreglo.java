import java.util.Arrays;

public class LongArreglo extends Arreglo {
    private long[] arr;
    private int size;

    LongArreglo(int capacidad) {
        arr = new long[capacidad];
        size = 0;
    }

    @Override
    public void ordenar() {
        Arrays.sort(arr, 0, size);
        for (int i = 0; i < size / 2; i++) {
            long temp = arr[i];
            arr[i] = arr[size - 1 - i];
            arr[size - 1 - i] = temp;
        }

    }

    @Override
    public void imprimir() {
        System.out.println("Array ordenado: ");
        for (int i = 0; i< size; i++){
            System.out.println(arr[i]+ " ");
        }
        System.out.println();


    }


    public void insertar(long elemento) {
        if (size < arr.length) {
            arr[size++] = (long) elemento;
        }
    }
}
